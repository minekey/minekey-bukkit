package io.minekey.bukkit.protocol;

import org.bukkit.entity.Player;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * A PacketHandler handles incoming plugin messages, this allows for a cleaner looking approach
 */
public interface PacketHandler {
    /**
     * Handles incoming and outgoing data
     *
     * @param player The player
     * @param input The data read from the client
     * @param output The output to write to the client
     * @return Return true if we should send something to the client!
     */
    boolean handle(Player player, DataInputStream input, DataOutputStream output);
}
