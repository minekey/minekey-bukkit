package io.minekey.bukkit.protocol;

import io.minekey.bukkit.MineKeyBukkit;
import io.minekey.bukkit.utils.BukkitUtils;
import org.bukkit.conversations.Conversation;
import org.bukkit.entity.Player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * This handles the AUTH_RESPONSE received from the client,
 */
public class AuthResponseHandler implements PacketHandler {

    private MineKeyBukkit plugin;

    public AuthResponseHandler(MineKeyBukkit plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean handle(Player player, DataInputStream input, DataOutputStream output) {
        try {
            String responseString = input.readUTF();

            plugin.getProtocolStageStorage().remove(player.getUniqueId());

            if (plugin.getChallengeStorage().containsKey(player.getUniqueId())) {
                if (plugin.getChallengeStorage().get(player.getUniqueId()).equals(responseString)) {
                    plugin.getChallengeStorage().remove(player.getUniqueId());
                    player.removeMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY, plugin);

                    Conversation conversation = (Conversation) player.getMetadata("MINEKEY_CONVO").get(0).value();
                    player.abandonConversation(conversation);
                    player.removeMetadata("MINEKEY_CONVO", plugin);

                    if (!plugin.getConfig().getBoolean("minekey.silent")) {
                        player.sendMessage(BukkitUtils.color(plugin.getConfig().getString("minekey.messages.auth-success")));
                    }
                } else {
                    plugin.getChallengeStorage().remove(player.getUniqueId());
                    player.removeMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY, plugin);
                    BukkitUtils.asyncKick(player, plugin, plugin.getConfig().getString("minekey.messages.auth-failure"));
                }
            } else {
                // Shouldn't happen, but if it does...kick 'em! (Because something strange happened)
                BukkitUtils.asyncKick(player, plugin, plugin.getConfig().getString("minekey.messages.auth-failure"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
