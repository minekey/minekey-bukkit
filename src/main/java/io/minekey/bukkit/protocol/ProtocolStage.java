package io.minekey.bukkit.protocol;

import java.util.Arrays;
import java.util.List;

public enum ProtocolStage {

    HANDSHAKE("HS_COMPLETE"),
    AUTHENTICATION("AUTH_RESPONSE");


    List<String> messages;

    ProtocolStage(String... messages) {
        this.messages = Arrays.asList(messages);
    }

    public List<String> getMessages() {
        return messages;
    }

}
