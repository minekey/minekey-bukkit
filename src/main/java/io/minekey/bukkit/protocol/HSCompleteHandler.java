package io.minekey.bukkit.protocol;

import io.minekey.bukkit.MineKeyBukkit;
import io.minekey.bukkit.SecurityMode;
import io.minekey.bukkit.utils.BukkitUtils;
import io.minekey.bukkit.utils.HttpUtils;
import io.minekey.bukkit.utils.KeyUtils;
import io.minekey.bukkit.utils.SecurityUtils;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import javax.xml.bind.DatatypeConverter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.UUID;

/**
 * Handles the HS_COMPLETE message received from the client.
 * This will initiate the AUTH_CHALLENGE
 */
public class HSCompleteHandler implements PacketHandler {

    private MineKeyBukkit plugin;

    public HSCompleteHandler(MineKeyBukkit plugin) {
        this.plugin = plugin;
    }


    @Override
    public boolean handle(Player player, DataInputStream input, DataOutputStream output) {
        // Remove the metadata, the player has the mod
        player.removeMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY, plugin);
        // Add the unauthorized state key
        player.setMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY, new FixedMetadataValue(plugin, true));

        String url = MineKeyBukkit.BASE_API_URL + player.getUniqueId().toString()
                + (plugin.getSecurityMode() == SecurityMode.EXTREME ? "/" + player.getAddress().getAddress().getHostAddress() : "");
        try {
            String response = HttpUtils.makeGetRequest(url);
            if (response.equalsIgnoreCase("Unauthorized")) {
                BukkitUtils.asyncKick(player, plugin, plugin.getConfig().getString("minekey.messages.kick-unauthorized"));
            } else if (response.isEmpty()) {
                BukkitUtils.asyncKick(player, plugin, plugin.getConfig().getString("minekey.messages.key-not-found"));
            } else {
                byte[] keyData = DatatypeConverter.parseBase64Binary(KeyUtils.getBase64StringFromKey(response));
                PublicKey publicKey = SecurityUtils.getPublicKey(keyData, "RSA");
                String challenge = "success" + UUID.randomUUID().toString();

                plugin.getChallengeStorage().put(player.getUniqueId(), challenge);

                // Encrypt the data!
                byte[] encryptedChallenge = SecurityUtils.encryptRSA(challenge.getBytes(StandardCharsets.UTF_8), publicKey);

                output.writeUTF("AUTH_CHALLENGE");
                output.write(encryptedChallenge);
                plugin.getProtocolStageStorage().put(player.getUniqueId(), ProtocolStage.AUTHENTICATION);
                return true;
            }
            return false;
        } catch (IOException | GeneralSecurityException ex) {
            ex.printStackTrace();
            BukkitUtils.asyncKick(player, plugin, "&c" + ex.getClass().getName() + ": " + ex.getMessage());
            return false;
        }
    }
}
