package io.minekey.bukkit;

import io.minekey.bukkit.listeners.MineKeyMessageListener;
import io.minekey.bukkit.listeners.PlayerLoginListener;
import io.minekey.bukkit.listeners.PlayerQuitListener;
import io.minekey.bukkit.listeners.ServerProtectionListener;
import io.minekey.bukkit.protocol.ProtocolStage;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class MineKeyBukkit extends JavaPlugin {

    // The security mode of the server. Options are NORMAL, or EXTREME
    private SecurityMode securityMode;

    // The base API URL
    public static final String BASE_API_URL = "http://api.minekey.io/key/";

    // Metadata keys
    public static final String UNAUTHORIZED_STATE_META_KEY = "minekey-unauthorized";
    public static final String SEARCHING_MOD_META_KEY = "minekey-searching-mod";

    // Stores the challenge for the UUID while we wait for a response.
    private final Map<UUID, String> challengeStorage = new ConcurrentHashMap<>();

    // Stores the stage of the protocol the player is in
    private Map<UUID, ProtocolStage> uuidProtocolStageMap = new ConcurrentHashMap<>();


    @Override
    public void onEnable() {
        saveDefaultConfig();

        // be nice, and allow for lower case :)
        String mode = getConfig().getString("minekey.security-mode", "NORMAL").toUpperCase();

        securityMode = SecurityMode.valueOf(mode);

        Bukkit.getPluginManager().registerEvents(new PlayerLoginListener(this), this);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(this), this);
        Bukkit.getPluginManager().registerEvents(new ServerProtectionListener(this), this);
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "MineKey", new MineKeyMessageListener(this));
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "MineKey");


    }

    public SecurityMode getSecurityMode() {
        return securityMode;
    }

    public Map<UUID, String> getChallengeStorage() {
        return challengeStorage;
    }

    public Map<UUID, ProtocolStage> getProtocolStageStorage() {
        return uuidProtocolStageMap;
    }
}
