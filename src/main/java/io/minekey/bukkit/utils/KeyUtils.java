package io.minekey.bukkit.utils;

public final class KeyUtils {

    /**
     * Gets the Base64 encoded string from a SSH formatted string
     * (ssh-rsa <base64 string></base64>)
     *
     * @return The base64 encoded part of the SSH key
     */
    public static String getBase64StringFromKey(String sshKey) {
        // Split the spaces, and get the text at the index of 1
        return sshKey.split(" ")[1];
    }

}
