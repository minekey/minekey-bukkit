package io.minekey.bukkit.utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Contains multiple utilities for interacting easily with the java.security library
 *
 * @author rbrick
 */
public final class SecurityUtils {

    /**
     * Decrypt using RSA
     *
     * @see SecurityUtils#decrypt(byte[], Key, String)
     */
    public static byte[] decryptRSA(byte[] data, Key key) throws InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
        return decrypt(data, key, "RSA");
    }

    /**
     * Encrypt using RSA
     *
     * @see SecurityUtils#encrypt(byte[], Key, String)
     */
    public static byte[] encryptRSA(byte[] data, Key key) throws InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
        return encrypt(data, key, "RSA");
    }

    /**
     * @see SecurityUtils#crypt(byte[], Key, String, boolean)
     */
    public static byte[] decrypt(byte[] data, Key key, String algorithm) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return crypt(data, key, algorithm, false);
    }

    /**
     * @see SecurityUtils#crypt(byte[], Key, String, boolean)
     */
    public static byte[] encrypt(byte[] data, Key key, String algorithm) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return crypt(data, key, algorithm, true);
    }

    /**
     * Encrypts or decrypt the {@code data}, using the Key provided
     *
     * @param data      The data to encrypt/decrypt
     * @param key       The key to encrypt/decrypt with
     * @param algorithm The algorithm to use
     * @param encrypt   Whether to encrypt or decrypt the data
     * @return A byte array of the encrypted/decrypted data
     */
    private static byte[] crypt(byte[] data, Key key, String algorithm, boolean encrypt) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(data);
    }

    /**
     * @see SecurityUtils#getKey(byte[], String, boolean)
     */
    public static PrivateKey getPrivateKey(byte[] keyData, String algorithm) throws InvalidKeySpecException, NoSuchAlgorithmException {
        return (PrivateKey) getKey(keyData, algorithm, false);
    }

    /**
     * @see SecurityUtils#getKey(byte[], String, boolean)
     */
    public static PublicKey getPublicKey(byte[] keyData, String algorithm) throws InvalidKeySpecException, NoSuchAlgorithmException {
        return (PublicKey) getKey(keyData, algorithm, true);
    }

    /**
     * Create a {@link java.security.Key} from the provided {@code keyData}
     * <p>
     * See https://docs.oracle.com/javase/tutorial/security/apisign/vstep2.html
     *
     * @param keyData   The data for the key
     * @param algorithm The algorithm used to generate the key
     * @param publicKey Whether a public or private key should be created
     * @return A key
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     */
    private static Key getKey(byte[] keyData, String algorithm, boolean publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        KeySpec encodedKeySpec = publicKey ? new X509EncodedKeySpec(keyData) : new PKCS8EncodedKeySpec(keyData);
        return publicKey ? keyFactory.generatePublic(encodedKeySpec) : keyFactory.generatePrivate(encodedKeySpec);
    }

}
