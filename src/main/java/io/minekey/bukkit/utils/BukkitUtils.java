package io.minekey.bukkit.utils;

import org.bukkit.ChatColor;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Some useful Bukkit utilities
 */
public final class BukkitUtils {

    /**
     * Colors a message
     *
     * @param message The message to color
     * @return a colorized version of the provided message.
     */
    public static String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }


    /**
     * Provides an easy method of kicking a player during a AsyncTask
     * @param player The player
     */
    public static void asyncKick(final Player player, final JavaPlugin plugin, final String message) {
        new BukkitRunnable() {
            @Override
            public void run() {
                player.kickPlayer(color(message));
            }
        }.runTask(plugin);
    }


    public static NullPrompt createNullPrompt() {
        return new NullPrompt();
    }


    private static class NullPrompt implements Prompt {
        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "";
        }

        @Override
        public boolean blocksForInput(ConversationContext conversationContext) {
            return true;
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String s) {
            return this;
        }
    }

}
