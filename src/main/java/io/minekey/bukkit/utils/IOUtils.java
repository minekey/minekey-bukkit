package io.minekey.bukkit.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Various utilities related to IO
 */
public final class IOUtils {

    /**
     * Writes a String array to a Data output stream
     *
     * @param stream The stream to write to
     * @param data   The data
     */
    public static void writeStringArray(DataOutputStream stream, String[] data) throws IOException {
        for (String s : data) {
            stream.writeUTF(s);
        }
        stream.flush();
    }

    /**
     * Read a String array from a Data input stream
     *
     * @param inputStream The input stream
     * @param amount      The amount of strings to read
     * @return A string array
     */
    public static String[] readStringArray(DataInputStream inputStream, int amount) throws IOException {
        String[] result = new String[amount];

        for (int i = 0; i < amount; i++) {
            // Read the UTF from the stream
            result[i] = inputStream.readUTF();
        }

        return result;
    }


}
