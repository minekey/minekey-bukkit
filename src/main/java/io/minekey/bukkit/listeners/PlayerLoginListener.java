package io.minekey.bukkit.listeners;

import io.minekey.bukkit.MineKeyBukkit;
import io.minekey.bukkit.protocol.ProtocolStage;
import io.minekey.bukkit.utils.BukkitUtils;
import org.bukkit.Bukkit;
import org.bukkit.conversations.Conversation;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import static io.minekey.bukkit.utils.BukkitUtils.color;

public class PlayerLoginListener implements Listener {

    private MineKeyBukkit plugin;

    public PlayerLoginListener(MineKeyBukkit plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        final Player player = event.getPlayer();

        // Set the metadata key
        player.setMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY, new FixedMetadataValue(plugin, true));

        final Conversation conversation = new Conversation(plugin, player, BukkitUtils.createNullPrompt());
        conversation.setLocalEchoEnabled(false);

        player.beginConversation(conversation);
        player.setMetadata("MINEKEY_CONVO", new FixedMetadataValue(plugin, conversation));

        // Send the plugin message
        // Delay by half a second
        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                if (!plugin.getConfig().getBoolean("minekey.silent")) {
                    // Send the player the message
                    player.sendRawMessage(color(plugin.getConfig().getString("minekey.messages.initial-join")));
                }

                try {
                    // Write the Handshake message
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    DataOutputStream stream = new DataOutputStream(baos);

                    stream.writeUTF("HANDSHAKE");

                    stream.flush();
                    baos.flush();
                    // Close the streams
                    stream.close();
                    baos.close();

                    player.sendPluginMessage(plugin, "MineKey", baos.toByteArray());
                    plugin.getProtocolStageStorage().put(player.getUniqueId(), ProtocolStage.HANDSHAKE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 10);

        // We will wait to allow for the client to send us a response, if no response occurs during that time,
        // then we kick the player.
        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                player.abandonConversation(conversation);

                if (player.hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY)) {
                    // If the player requires MineKey installed
                    if (player.hasPermission("minekey.require")) {
                        player.kickPlayer(color(plugin.getConfig().getString("minekey.messages.no-mod-found")));
                    }
                    player.removeMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY, plugin);
                    plugin.getProtocolStageStorage().remove(player.getUniqueId());
                }
            }
        }, plugin.getConfig().getInt("minekey.auth-timeout") * 20 + 10);


    }


}
