package io.minekey.bukkit.listeners;

import io.minekey.bukkit.MineKeyBukkit;
import io.minekey.bukkit.protocol.AuthResponseHandler;
import io.minekey.bukkit.protocol.HSCompleteHandler;
import io.minekey.bukkit.protocol.PacketHandler;
import io.minekey.bukkit.protocol.ProtocolStage;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static io.minekey.bukkit.utils.BukkitUtils.color;

/**
 * This class listens for plugin messages received from the client.
 */
public class MineKeyMessageListener implements PluginMessageListener {

    private MineKeyBukkit plugin;

    private Map<String, PacketHandler> handlerMap = new ConcurrentHashMap<>();


    public MineKeyMessageListener(MineKeyBukkit plugin) {
        this.plugin = plugin;
        handlerMap.put("HS_COMPLETE", new HSCompleteHandler(plugin));
        handlerMap.put("AUTH_RESPONSE", new AuthResponseHandler(plugin));
    }

    @Override
    public void onPluginMessageReceived(String channel, final Player player, byte[] message) {
        // Ensure that this is the proper channel
        if (channel.equals("MineKey") && (player.hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || player.hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY))) {

            // If the player is in the ProtocolStage map
            if (plugin.getProtocolStageStorage().containsKey(player.getUniqueId())) {
                // Start reading the response
                ByteArrayInputStream bais = new ByteArrayInputStream(message);
                final DataInputStream stream = new DataInputStream(bais);

                // We will have a concept similar to BungeeCord in the sense that there will be 'sub-channels'
                try {
                    final String subChannel = stream.readUTF();
                    ProtocolStage stage = plugin.getProtocolStageStorage().get(player.getUniqueId());

                    if (handlerMap.containsKey(subChannel) && stage.getMessages().contains(subChannel)) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                DataOutputStream dos = new DataOutputStream(baos);
                                // Handle the channel
                                if (handlerMap.get(subChannel).handle(player, stream, dos)) {

                                    // Flush & close the streams after handling
                                    try {
                                        dos.flush();
                                        baos.flush();
                                        dos.close();
                                        baos.close();

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    player.sendPluginMessage(plugin, "MineKey", baos.toByteArray());
                                }
                            }
                        }.runTaskAsynchronously(plugin);
                    } else {
                        // Kick the player if an abnormal message is received
                        player.kickPlayer(color(plugin.getConfig().getString("minekey.messages.abnormal")));
                        throw new IllegalArgumentException("Unrecognized Sub-Channel: " + subChannel);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
