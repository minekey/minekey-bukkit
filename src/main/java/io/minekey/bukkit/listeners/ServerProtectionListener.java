package io.minekey.bukkit.listeners;

import io.minekey.bukkit.MineKeyBukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * This listener will cancel things like the PlayerMoveEvent, BlockBreak/PlaceEvent, PlayerPreprocessCommandEvent,
 * PlayerTeleportEvent etc. while a player is in an unauthorized state (Including the Searching for mod state) to prohibit
 * any actions that could be used to "grief" or harm the server in anyway from a potentially un-wanted user.
 */
public class ServerProtectionListener implements Listener {

    private MineKeyBukkit plugin;

    public ServerProtectionListener(MineKeyBukkit plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            if ((event.getTo().getX() != event.getFrom().getX()) || (event.getTo().getY() != event.getFrom().getY()) || (event.getTo().getZ() != event.getFrom().getZ())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryOpen(InventoryOpenEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        if ((event.getDamager() instanceof Player) && event.getDamager().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getDamager().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        if (event.getPlayer().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getPlayer().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getWhoClicked().hasMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY) || event.getWhoClicked().hasMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY)) {
            event.setCancelled(true);
        }
    }

}
