package io.minekey.bukkit.listeners;

import io.minekey.bukkit.MineKeyBukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    private MineKeyBukkit plugin;

    public PlayerQuitListener(MineKeyBukkit plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.getPlayer().removeMetadata(MineKeyBukkit.SEARCHING_MOD_META_KEY, plugin);
        event.getPlayer().removeMetadata(MineKeyBukkit.UNAUTHORIZED_STATE_META_KEY, plugin);
        plugin.getChallengeStorage().remove(event.getPlayer().getUniqueId());
    }

}
